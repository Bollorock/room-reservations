package cz.cvut.fel.ds.roomreservations.util;

/**
 *
 * @author OndÅ™ej NetÃ­k <ondrej.netik@gmail.com>
 */
public class Values {
    
    
   
    public static final int WINDOW_HEIGHT = 480;
    public static final int WINDOW_WIDTH = 600;
    public static final int DIALOG_HEIGHT = 300;
    public static final int DIALOG_WIDTH = 400;
    
    public static final int PANEL_MAIN = 101;
    public static final int PANEL_LOGIN = 102;
    public static final int PANEL_REGISTER = 103;
    public static final int PANEL_ROLES = 104;
    public static final int PANEL_EQ = 105;
    public static final int PANEL_EQTYPES = 106;
    public static final int PANEL_RESERVE_CREATE = 1070;
    public static final int PANEL_RESERVE_OWN = 1071;
    public static final int PANEL_RESERVE_MANAGE = 108;
    public static final int PANEL_ROOMS = 109;
    
    public static final String PERMISSION_RESERVE_CREATE = "reserve_create";
    public static final String PERMISSION_RESERVE_EDIT = "reserve_edit";
    public static final String PERMISSION_ROOM_MANAGE = "room_manage";
    public static final String PERMISSION_USER_EDIT = "user_edit";
    public static final String PERMISSION_EQUIPMENT_MANAGE = "equipment_manage";
    public static final String PERMISSION_USER_REGISTER = "user_register";
    public static final String PERMISSION_ROLES_MANAGE = "roles_manage";
}
