package cz.cvut.fel.ds.roomreservations.util;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class GUIUtil {

    public static void displayError(String err) {
        JDialog jDialog = new JDialog();
        jDialog.setLocationRelativeTo(null);
        jDialog.setModal(true);
        JOptionPane.showMessageDialog(jDialog,
                err,
                "Error",
                JOptionPane.ERROR_MESSAGE);
    }

    public static void displayOK(String msg) {
        JDialog jDialog = new JDialog();
        jDialog.setLocationRelativeTo(null);
        jDialog.setModal(true);
        JOptionPane.showMessageDialog(jDialog,
                msg,
                "Success",
                JOptionPane.INFORMATION_MESSAGE);
    }

}
