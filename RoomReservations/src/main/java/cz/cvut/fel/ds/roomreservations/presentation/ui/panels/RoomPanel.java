/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.presentation.ui.panels;

import cz.cvut.fel.ds.roomreservations.presentation.controller.RoomController;
import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class RoomPanel extends JPanel {
    private static final long serialVersionUID = -1386479930594103802L;

    private JPanel panelButtons;
    private JScrollPane panelTable;
    private JTable table;
    private JPanel panelButtons2;

    private JButton buttonAdd;
    private JButton buttonEdit;
    private JButton buttonBack;
    
    private Object[][] rowData;

    public RoomPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        initComponent();
        this.setVisible(true);
    }

    private void initComponent() {
        removeAll();
        panelButtons = new JPanel(new FlowLayout());
        panelButtons2 = new JPanel(new FlowLayout());

        buttonAdd = new JButton("Add new room");
        buttonEdit = new JButton("Edit room");
        buttonBack = new JButton("Back");

        initTable();

        panelButtons.add(buttonAdd);
        panelButtons.add(buttonEdit);
        panelButtons2.add(buttonBack);
        
        add(panelButtons);
        add(panelTable);
        add(panelButtons2);
        
        addActionListeners();
    }

    private void initTable() {
        rowData = RoomController.getRowData(RoomController.getAllRooms());
        
        Object columns[] = {"room_id", "room_name", "room_capacity", "room_available", "price_base"};


        table = new JTable(rowData,columns);

        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        panelTable = new JScrollPane(table);
    }

    private void addActionListeners() {
        buttonBack.addActionListener((ActionEvent ae) -> {
            PanelSwitcher.switchPanel(Values.PANEL_MAIN);
        });
    }

}
