/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.presentation.controller;

import cz.cvut.fel.ds.roomreservations.persistance.dao.PersonDAO;
import cz.cvut.fel.ds.roomreservations.persistance.dao.PriceModifierDAO;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.EquipmentDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.PersonDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.PriceModifierDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.ReservationDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.RoomDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.TimeWindowDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.model.Equipment;
import cz.cvut.fel.ds.roomreservations.persistance.model.Person;
import cz.cvut.fel.ds.roomreservations.persistance.model.PriceModifer;
import cz.cvut.fel.ds.roomreservations.persistance.model.Reservation;
import cz.cvut.fel.ds.roomreservations.persistance.model.ReservationStatus;
import cz.cvut.fel.ds.roomreservations.persistance.model.Room;
import cz.cvut.fel.ds.roomreservations.persistance.model.TimeWindow;
import cz.cvut.fel.ds.roomreservations.presentation.ui.SessionContext;
import cz.cvut.fel.ds.roomreservations.util.DateUtils;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class ReserveController {

    private static final ReservationDAOImpl resdao = new ReservationDAOImpl();
    private static final TimeWindowDAOImpl twdao = new TimeWindowDAOImpl();
    private static final RoomDAOImpl roomdao = new RoomDAOImpl();
    private static final EquipmentDAOImpl eqDAO = new EquipmentDAOImpl();
    private static final PersonDAO perDAO = new PersonDAOImpl();
    private static final PriceModifierDAO priceModDAO = new PriceModifierDAOImpl();

    public static List<Reservation> getUserReservations(Person p) {
        return resdao.getReservationsByUser(p.getId());
    }

    public static List<Reservation> getAllReservations() {
        return resdao.getAllReservations();
    }

    public static List<TimeWindow> getAllTimeWindows() {
        return twdao.getAllTimeWindows();
    }

    public static List<TimeWindow> getFreeTimeWindows(Date date, Room room) {
        List<TimeWindow> allTimeWindows = twdao.getAllTimeWindows();
        List<Reservation> allReservations = resdao.getReservationsByRoom(room.getId());
        List<TimeWindow> results = new ArrayList();
        results.addAll(allTimeWindows);

        HashSet<TimeWindow> reserved = new HashSet<>();
        allReservations.stream()
                .filter((r) -> (DateUtils.isSameDay(r.getDate(), date)))
                .filter((r) -> (r.getStatus().ordinal() > ReservationStatus.CANCELLED.ordinal()))
                .forEach((r) -> {
                    reserved.addAll(r.getTimeWindows());
                });

        results.removeAll(reserved);
        return results;
    }

    public static int getReservationPrice(Date date, Set<TimeWindow> times, Room room) {
        int timePrice = room.getBasePrice();
        List<Equipment> equipmentsByRoom = eqDAO.getEquipmentsByRoom(room.getId());
        for (Equipment eq : equipmentsByRoom) {
            timePrice += eq.getType().getAddedCreditValue();
        }
        int price = 0;
        for (TimeWindow time : times) {
            double modifier = getModifier(date, room, time);
            price += timePrice * modifier;
        }
        return price;
    }

    public static boolean makeReservation(Date date, Set<TimeWindow> times, Room room, String note, int cost) {
        Reservation r = buildReservation(
                date,
                SessionContext.getLoggedIn(),
                room,
                ReservationStatus.PENDING,
                times,
                note,
                cost);

        if (resdao.addReservation(r)) {
            SessionContext.getLoggedIn().setCredits(SessionContext.getLoggedIn().getCredits() - cost);
            return true;
        } else {
            return false;
        }
    }

    private static Reservation buildReservation(Date date, Person p, Room room, ReservationStatus status, Set<TimeWindow> times, String note, int cost) {
        Reservation r = new Reservation();
        r.setDate(date);
        r.setPerson(p);
        r.setRoom(room);
        r.setStatus(status);
        r.setTimeWindows(times);
        r.setNote(note);
        r.setCost(cost);
        return r;
    }

    public static Reservation getReservationByID(int id) {
        return resdao.getReservationByID(id);
    }

    public static boolean updateReservation(int id) {
        return updateReservation(getReservationByID(id));
    }

    public static boolean updateReservation(Reservation reservation) {
        return resdao.updateReservation(reservation);
    }

    public static boolean cancelReservation(int id) {
        return cancelReservation(getReservationByID(id));
    }

    public static boolean cancelReservation(Reservation reservation) {
        reservation.setStatus(ReservationStatus.CANCELLED);
        return updateReservation(reservation);
    }

    public static Object[][] getRowData(List<Reservation> allReseservations) {
        Object rowData[][] = new Object[allReseservations.size()][6];
        for (int i = 0; i < allReseservations.size(); i++) {
            rowData[i][0] = allReseservations.get(i).getId();
            rowData[i][1] = allReseservations.get(i).getDate();
            rowData[i][2] = allReseservations.get(i).getNote();
            rowData[i][3] = allReseservations.get(i).getStatus();
            rowData[i][4] = allReseservations.get(i).getRoom();
            rowData[i][5] = allReseservations.get(i).getCost();
            //TODO: Fix display of timewindows
//            rowData[i][5] = allReseservations.get(i).getTimeWindows().toString(); 
        }
        return rowData;
    }

    private static double getModifier(Date date, Room room, TimeWindow time) {
        PriceModifer mod = priceModDAO.getPriceModifier(DateUtils.getDayOfWeek(date), time.getId(), room.getId());
        return mod == null ? 1 : mod.getPriceModifier();
    }
}
