/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.presentation.ui.dialogs;

import cz.cvut.fel.ds.roomreservations.persistance.model.Reservation;
import cz.cvut.fel.ds.roomreservations.persistance.model.ReservationStatus;
import cz.cvut.fel.ds.roomreservations.presentation.controller.ReserveController;
import cz.cvut.fel.ds.roomreservations.util.GUIUtil;
import cz.cvut.fel.ds.roomreservations.util.JTextFieldLimit;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.joda.time.DateTime;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class ReserveEditDialog extends JDialog {

    private final Reservation reservation;
    private final boolean fullAccess;

    private JPanel mainPanel;
    private JPanel panelButtons;
    private JPanel panelName;
    private JPanel panelDate;
    private JPanel panelRoom;
    private JPanel panelNote;
    private JPanel panelStatus;

    private JTextField textName;
    private JTextField textDate;
    private JTextField textRoom;
    private JTextArea textNote;
    private JComboBox comboStatus;

    private JButton buttonOK;
    private JButton buttonBack;

    public ReserveEditDialog(Reservation reservation, boolean fullAccess) {
        this.reservation = reservation;
        this.fullAccess = fullAccess;

        this.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        this.setModal(true);
        this.setLocationRelativeTo(null);
        setSize(new Dimension(Values.DIALOG_WIDTH + 200, Values.DIALOG_HEIGHT + 200));

        setTitle("Edit Reservation");

        initComponent();
        
        pack();
        
        this.setVisible(true);
    }

    private void initComponent() {
        mainPanel = new JPanel(new FlowLayout());
        panelButtons = new JPanel(new FlowLayout());
        panelName = new JPanel(new FlowLayout());
        panelDate = new JPanel(new FlowLayout());
        panelRoom = new JPanel(new FlowLayout());
        panelNote = new JPanel(new FlowLayout());
        panelStatus = new JPanel(new FlowLayout());

        String name = reservation.getPerson().getFirstName() + " " + reservation.getPerson().getLastName();
        textName = new JTextField(name);
        textName.setEnabled(false);

        String date = (new DateTime(reservation.getDate()).toString("dd. MM."));
        textDate = new JTextField(date);
        textDate.setEnabled(false);

        String room = reservation.getRoom().getName();
        textRoom = new JTextField(room);
        textRoom.setEnabled(false);

        String note = reservation.getNote();
        textNote = new JTextArea(4, 30);
        textNote.setLineWrap(true);
        textNote.setPreferredSize(textNote.getSize());
        textNote.setDocument(new JTextFieldLimit(120));
        textNote.setText(note);
        textNote.setEnabled(true);

        //String[] stats = ReservationStatus.getAsStringArray();
        ReservationStatus[] values = ReservationStatus.values();
        comboStatus = new JComboBox(values);
        comboStatus.setSelectedItem(reservation.getStatus());
        comboStatus.setEnabled(fullAccess);

        panelName.add(new JLabel("Jméno:"));
        panelName.add(textName);

        panelDate.add(new JLabel("Datum:"));
        panelDate.add(textDate);

        panelRoom.add(new JLabel("Místnost:"));
        panelRoom.add(textRoom);

        panelNote.add(new JLabel("Poznámka:"));
        panelNote.add(textNote);

        panelStatus.add(new JLabel("Status:"));
        panelStatus.add(comboStatus);

        buttonOK = new JButton("Confirm");
        buttonBack = new JButton("Cancel");

        panelButtons.add(buttonOK);
        panelButtons.add(buttonBack);

        //add(mainPanel);
        add(panelName);
        add(panelDate);
        add(panelRoom);
        add(panelNote);
        add(panelStatus);
        add(panelButtons);

        addActionListeners();

    }

    private void addActionListeners() {
        buttonOK.addActionListener((ActionEvent ae) -> {
            reservation.setNote(textNote.getText());
            reservation.setStatus((ReservationStatus) comboStatus.getSelectedItem());
            boolean update = ReserveController.updateReservation(reservation);
            if (update) {
                GUIUtil.displayOK("Reservation updated successfully.");
                this.dispose();
            } else {
                GUIUtil.displayError("Reservation update failed.");
            }
        });

        buttonBack.addActionListener((ActionEvent ae) -> {
            this.dispose();
        });
    }

}
