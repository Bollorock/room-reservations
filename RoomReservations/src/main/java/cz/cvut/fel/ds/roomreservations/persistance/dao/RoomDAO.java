/*
 * Copyright (C) 2014 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao;

import cz.cvut.fel.ds.roomreservations.persistance.model.Room;
import java.util.List;

/**
 * Data Access Object for Room entity.
 * 
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public interface RoomDAO {

    /**
     * Retrives room with specified id.
     *
     * @param roomId Desired id
     * @return desired room or null if room with specified id does not exist
     */
    Room getRoomByID(Integer roomId);

    /**
     * Retrives room with specified roomname.
     *
     * @param name Desired room name
     * @return desired room or null if room with specified roomname does
     * not exist
     */
    Room getRoomByName(String name);

    /**
     * Adds room to the database. Room id can be set to specific value, or
     * null to generate new id, generated id is set to the object.
     *
     * @param room
     * @return true if room was successfully added
     */
    boolean addRoom(Room room);

    /**
     * Updates room in database.
     *
     * @param room Room to update, with id different form null
     * @return true if room was successfully updated
     */
    boolean updateRoom(Room room);

    /**
     * Deletes room from database.
     *
     * @param roomId Id of room to delete, can't be null
     * @return true if room was successfully deleted
     */
    boolean deleteRoom(Integer roomId);

    /**
     * Retrieves all rooms from database.
     *
     * @return list of rooms
     */
    List<Room> getAllRooms();

}
