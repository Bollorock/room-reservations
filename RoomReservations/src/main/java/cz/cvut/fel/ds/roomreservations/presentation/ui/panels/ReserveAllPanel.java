/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.presentation.ui.panels;

import cz.cvut.fel.ds.roomreservations.presentation.controller.ReserveController;
import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.presentation.ui.dialogs.ReserveEditDialog;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class ReserveAllPanel extends JPanel {
    private static final long serialVersionUID = 112034958130536924L;

    private JScrollPane panelTable;
    private JTable table;
    private JPanel panelButtons;
    private JPanel panelButtons2;

    private JButton buttonEdit;
    private JButton buttonBack;

    private Object[][] rowData;

    public ReserveAllPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        initComponent();
        this.setVisible(true);
    }

    private void initComponent() {
        removeAll();
        panelButtons = new JPanel(new FlowLayout());
        panelButtons2 = new JPanel(new FlowLayout());
        buttonEdit = new JButton("Edit");
        buttonBack = new JButton("Back");

        panelButtons.add(buttonEdit);
        panelButtons2.add(buttonBack);

        initTable();

        add(panelButtons);
        add(panelTable);
        add(panelButtons2);
        addActionListeners();
    }

    private void initTable() {
        rowData = ReserveController.getRowData(ReserveController.getAllReservations());
        Object columns[] = {"reservation_id", "date", "note", "status", "room_id", "cost"};

        table = new JTable(rowData, columns);

        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        panelTable = new JScrollPane(table);
    }

    private void addActionListeners() {
        buttonEdit.addActionListener((ActionEvent ae) -> {
            int selectedRow = table.getSelectedRow();
            int selectedID = (int) rowData[selectedRow][0];
            ReserveEditDialog reserveEditFrame = new ReserveEditDialog(ReserveController.getReservationByID(selectedID), true);
            initComponent();
            repaint();
            revalidate();
        });

        buttonBack.addActionListener((ActionEvent ae) -> {
            PanelSwitcher.switchPanel(Values.PANEL_MAIN);
        });
    }

}
