package cz.cvut.fel.ds.roomreservations.presentation.ui;

import cz.cvut.fel.ds.roomreservations.persistance.model.Person;
import cz.cvut.fel.ds.roomreservations.persistance.model.Role;
import java.util.Set;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class SessionContext {
    
    private static Person loggedIn;
    
    /**
     * Upon successful login, stores Person logged in.
     * 
     * @param p 
     */
    public static void setLoggedIn(Person p){
        SessionContext.loggedIn = p;
    }
    
    public static Person getLoggedIn(){
      
        return SessionContext.loggedIn;
    }
    
    public static Set<Role> getLoggedInRoles(){
        
        return getLoggedIn().getRoles();
    }
}
