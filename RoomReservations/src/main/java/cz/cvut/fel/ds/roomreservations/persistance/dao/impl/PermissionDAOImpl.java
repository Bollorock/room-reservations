/*
 * Copyright (C) 2015 Ond?ej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.persistance.dao.PermissionDAO;
import cz.cvut.fel.ds.roomreservations.persistance.model.Permission;
import java.util.List;

public class PermissionDAOImpl extends CommonDAOImpl<Permission> implements PermissionDAO {

    public PermissionDAOImpl() {
        super(Permission.class);
    }

    @Override
    public Permission getPermissionByID(Integer permissionId) {
        return getEntityById(permissionId);
    }

    @Override
    public Permission getPermissionByName(String name) {
        return getFirstEntityByColumn("name", name);
    }

    @Override
    public boolean addPermission(Permission permission) {
        return addEntity(permission);
    }

    @Override
    public boolean updatePermission(Permission permission) {
        return updateEntity(permission);
    }

    @Override
    public boolean deletePermission(Integer permissionId) {
        return deleteEntity(permissionId);
    }

    @Override
    public List<Permission> getAllPermissions() {
        return getAllEntities();
    }

}
