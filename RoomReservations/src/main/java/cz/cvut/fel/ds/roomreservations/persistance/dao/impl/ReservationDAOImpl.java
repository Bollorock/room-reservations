/*
 * Copyright (C) 2015 Ond?ej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.persistance.dao.ReservationDAO;
import cz.cvut.fel.ds.roomreservations.persistance.model.Reservation;
import java.util.List;


public class ReservationDAOImpl extends CommonDAOImpl<Reservation> implements ReservationDAO {

    public ReservationDAOImpl() {
        super(Reservation.class);
    }

    @Override
    public Reservation getReservationByID(Integer reservationId) {
        return getEntityById(reservationId);
    }

    @Override
    public boolean addReservation(Reservation reservation) {
        return addEntity(reservation);
    }

    @Override
    public boolean updateReservation(Reservation reservation) {
        return updateEntity(reservation);
    }

    @Override
    public boolean deleteReservation(Integer reservationId) {
        return deleteEntity(reservationId);
    }

    @Override
    public List<Reservation> getAllReservations() {
        return getAllEntities();
    }

    @Override
    public List<Reservation> getReservationsByUser(Integer personId) {
        return getAllEntityByColumn("person.id", personId);
    }

    @Override
    public List<Reservation> getReservationsByRoom(Integer roomId) {
        return getAllEntityByColumn("room.id", roomId);
    }

}
