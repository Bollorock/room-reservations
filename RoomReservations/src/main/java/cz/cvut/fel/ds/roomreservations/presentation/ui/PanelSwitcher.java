package cz.cvut.fel.ds.roomreservations.presentation.ui;

import cz.cvut.fel.ds.roomreservations.presentation.ui.frames.MainFrame;
import cz.cvut.fel.ds.roomreservations.presentation.ui.panels.*;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class PanelSwitcher {

    private static MainFrame mainFrame;

    private static JPanel actualPanel;

    public static void setMainFrame(MainFrame mainFrame) {
        PanelSwitcher.mainFrame = mainFrame;
    }

    public static void switchPanel(int panel) {

        switch (panel) {
            case Values.PANEL_MAIN:
                actualPanel = new MainPanel();
                break;

            case Values.PANEL_LOGIN:
                actualPanel = new LoginPanel();
                break;

            case Values.PANEL_REGISTER:
                actualPanel = new RegisterPanel();
                break;
                
            case Values.PANEL_ROOMS:
                actualPanel = new RoomPanel();
                break;
            
            case Values.PANEL_ROLES:
                actualPanel = new RolePanel();
                break;
                
            case Values.PANEL_RESERVE_CREATE:
                actualPanel = new ReserveCreatePanel();
                break;
            
            case Values.PANEL_RESERVE_OWN:
                actualPanel = new ReserveOwnPanel();
                break;
            
            case Values.PANEL_RESERVE_MANAGE:
                actualPanel = new ReserveAllPanel();
                break;
                
            default: {
                Logger.getLogger(PanelSwitcher.class.getName()).log(Level.SEVERE, "SWITCHING TO UNKNOWN PANEL");
                //throw new UnknownError("SWITCHING TO UNKNOWN PANEL");
            }
            
            
        }
        
        mainFrame.getContentPane().removeAll();
        mainFrame.getContentPane().add(PanelSwitcher.actualPanel);
        
        mainFrame.repaint();
        mainFrame.revalidate();
    }

}
