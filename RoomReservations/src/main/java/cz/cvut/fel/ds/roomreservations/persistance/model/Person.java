/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ds.roomreservations.persistance.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.DataClass;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
@Entity(name = "Person")
@Table(name = "Person")
public class Person implements DataClass, Serializable {

    private static final long serialVersionUID = 3484596893080431515L;
    
    @Id
    @Column(name = "person_id", unique = true, nullable = false)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(name = "first_name", nullable = false, unique = false, length = 64)
    private String firstName;

    @Column(name = "last_name", nullable = false, unique = false, length = 64)
    private String lastName;

    @Column(name = "email", nullable = false, unique = true, length = 64)
    private String email;

    @Column(name = "username", nullable = false, unique = true, length = 64)
    private String username;

    @Column(name = "pwd", nullable = false, unique = false, length = 255)
    private String password;

    @Column(name = "credits", nullable = false, unique = false)
    private int credits = 0;

    @Column(name = "weekly_income", nullable = false, unique = false)
    private int weaklyIncome = 0;

    @ManyToMany
    @JoinTable(name = "person_role",
            joinColumns = {
                @JoinColumn(name = "person_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "role_id")})
    private Set<Role> roles;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getWeaklyIncome() {
        return weaklyIncome;
    }

    public void setWeaklyIncome(int weaklyIncome) {
        this.weaklyIncome = weaklyIncome;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(this);
    }

}
