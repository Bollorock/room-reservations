/*
 * Copyright (C) 2014 Ladislav Mejzlík <lmejzlik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao;

import cz.cvut.fel.ds.roomreservations.persistance.model.EquipmentType;
import java.util.List;

/**
 * Data Acces Object for EquipmentType entity.
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public interface EquipmentTypeDAO {

    /**
     * Retrives equipmenttype with specified id.
     *
     * @param equipmenttypeId Desired id
     * @return desired equipmenttype or null if equipmenttype with specified id does not exist
     */
    EquipmentType getEquipmentTypeByID(Integer equipmenttypeId);

    /**
     * Retrives equipmenttype with specified equipmenttypename.
     *
     * @param name
     * @return desired equipmenttype or null if equipmenttype with specified equipmenttypename does
     * not exist
     */
    EquipmentType getEquipmentTypeByName(String name);

    /**
     * Adds equipmenttype to the database. EquipmentType id can be set to specific value, or
     * null to generate new id, generated id is set to the object.
     *
     * @param equipmenttype
     * @return true if equipmenttype was successfully added
     */
    boolean addEquipmentType(EquipmentType equipmenttype);

    /**
     * Updates equipmenttype in database.
     *
     * @param equipmenttype EquipmentType to update, with id different form null
     * @return true if equipmenttype was successfully updated
     */
    boolean updateEquipmentType(EquipmentType equipmenttype);

    /**
     * Deletes equipmenttype from database.
     *
     * @param equipmenttypeId Id of equipmenttype to delete, can't be null
     * @return true if equipmenttype was successfully deleted
     */
    boolean deleteEquipmentType(Integer equipmenttypeId);

    /**
     * Retrieves all equipmenttypes from database.
     *
     * @return list of equipmenttypes
     */
    List<EquipmentType> getAllEquipmentTypes();

}
