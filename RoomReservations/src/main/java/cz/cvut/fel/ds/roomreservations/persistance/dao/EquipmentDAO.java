/*
 * Copyright (C) 2014 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao;

import cz.cvut.fel.ds.roomreservations.persistance.model.Equipment;
import java.util.List;

/**
 * Data Access Object for Equipment entity.
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public interface EquipmentDAO {

    /**
     * Retrives equipment with specified id.
     *
     * @param equipmentId Desired id
     * @return desired equipment or null if equipment with specified id does not
     * exist
     */
    Equipment getEquipmentByID(Integer equipmentId);

    List<Equipment> getEquipmentsByRoom(Integer roomId);

    /**
     * Adds equipment to the database. Equipment id can be set to specific
     * value, or null to generate new id, generated id is set to the object.
     *
     * @param equipment
     * @return true if equipment was successfully added
     */
    boolean addEquipment(Equipment equipment);

    /**
     * Updates equipment in database.
     *
     * @param equipment Equipment to update, with id different form null
     * @return true if equipment was successfully updated
     */
    boolean updateEquipment(Equipment equipment);

    /**
     * Deletes equipment from database.
     *
     * @param equipmentId Id of equipment to delete, can't be null
     * @return true if equipment was successfully deleted
     */
    boolean deleteEquipment(Integer equipmentId);

    /**
     * Retrieves all equipments from database.
     *
     * @return list of equipments
     */
    List<Equipment> getAllEquipments();

}
