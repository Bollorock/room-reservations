package cz.cvut.fel.ds.roomreservations.presentation.controller;

import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.PersonDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.RoleDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.model.Person;
import cz.cvut.fel.ds.roomreservations.persistance.model.Role;
import cz.cvut.fel.ds.roomreservations.util.PasswordHash;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class RegisterController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    private static final PersonDAOImpl pdao = new PersonDAOImpl();
    private static final RoleDAOImpl rdao = new RoleDAOImpl();

    /**
     * Registers a user to the system.
     *
     * TODO: Currently zero checking. Needs fixing.
     *
     * @param firstName
     * @param lastName
     * @param email
     * @param username
     * @param password
     * @return
     */
    public static boolean register(String firstName, String lastName, String email, String username, char[] password) {
        if (firstName.equals("") || lastName.equals("") || email.equals("") || username.equals("") || password.length < 1) {
            return false;
        }
        try {
            Person p = new Person();
            p.setFirstName(firstName);
            p.setLastName(lastName);
            p.setEmail(email);
            p.setUsername(username);
            p.setPassword(PasswordHash.createHash(password));
            
            Role r = rdao.getRoleByName("empty"); //Assigns permissionless role "empty"
            Set<Role> rset = new HashSet<>();
            rset.add(r);
            
            p.setRoles(rset);

            return pdao.addPerson(p);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

}
