/*
 * Copyright (C) 2014 Ladislav Mejzlík <lmejzlik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao;

import cz.cvut.fel.ds.roomreservations.persistance.model.Person;
import java.util.List;

/**
 * Data Acces Object for Person entity.
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public interface PersonDAO {

    /**
     * Retrives person with specified id.
     *
     * @param personId Desired id
     * @return desired person or null if person with specified id does not exist
     */
    Person getPersonByID(Integer personId);

    /**
     * Retrives person with specified personname.
     *
     * @param username Desired personname
     * @return desired person or null if person with specified personname does
     * not exist
     */
    Person getPersonByUsername(String username);

    /**
     * Adds person to the database. Person id can be set to specific value, or
     * null to generate new id, generated id is set to the object.
     *
     * @param person
     * @return true if person was successfully added
     */
    boolean addPerson(Person person);

    /**
     * Updates person in database.
     *
     * @param person Person to update, with id different form null
     * @return true if person was successfully updated
     */
    boolean updatePerson(Person person);

    /**
     * Deletes person from database.
     *
     * @param personId Id of person to delete, can't be null
     * @return true if person was successfully deleted
     */
    boolean deletePerson(Integer personId);

    /**
     * Retrieves all persons from database.
     *
     * @return list of persons
     */
    List<Person> getAllPersons();

}
