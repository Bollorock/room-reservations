/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ds.roomreservations.presentation.ui.panels;

import cz.cvut.fel.ds.roomreservations.presentation.controller.LoginController;
import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.util.GUIUtil;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class LoginPanel extends JPanel {
    private static final long serialVersionUID = 8582591937438188321L;

    private JPanel panelUser;
    private JPanel panelPass;

    private JPanel buttonPanel;

    private JButton buttonOK;
    private JButton buttonExit;

    private JLabel labelUser;
    private JLabel labelPass;

    private JTextField textFieldUser;
    private JPasswordField textFieldPass;

    public LoginPanel() {

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        initComponent();
        this.setVisible(true);
    }

    private void initComponent() {

        this.panelUser = new JPanel(new FlowLayout());
        this.panelPass = new JPanel(new FlowLayout());

        buttonPanel = new JPanel(new FlowLayout());

        buttonOK = new JButton("OK");
        buttonExit = new JButton("EXIT");

        labelUser = new JLabel("Username:");
        labelPass = new JLabel("Password");

        textFieldUser = new JTextField(20);
        textFieldUser.setToolTipText("Username");
       // textFieldUser.setPreferredSize(new Dimension(200, 30));
        textFieldUser.setHorizontalAlignment(JTextField.CENTER);

        textFieldPass = new JPasswordField(20);
        textFieldPass.setToolTipText("Password:");
        //textFieldPass.setPreferredSize(new Dimension(200, 30));
        textFieldPass.setHorizontalAlignment(JTextField.CENTER);
        textFieldPass.setEchoChar('*');

        addActionListeners();

        panelUser.add(labelUser);
        panelUser.add(textFieldUser);
        panelPass.add(labelPass);
        panelPass.add(textFieldPass);

        buttonPanel.add(buttonOK);
        buttonPanel.add(buttonExit);

        add(panelUser);
        add(panelPass);
        add(buttonPanel);

    }

    private void addActionListeners() {
        buttonOK.addActionListener((ActionEvent ae) -> {
            buttonOK.setEnabled(false);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            String userName = textFieldUser.getText();
            char[] password = textFieldPass.getPassword();
//                System.out.println(hashedPass);

            if (LoginController.login(userName, password)) {

                PanelSwitcher.switchPanel(Values.PANEL_MAIN);

            } else {
                textFieldUser.setText("");
                textFieldPass.setText("");
                GUIUtil.displayError("Invalid username or password.");
            }
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            buttonOK.setEnabled(true);
        });

        buttonExit.addActionListener((ActionEvent ae) -> {
            System.exit(0);
        });

    }

}
