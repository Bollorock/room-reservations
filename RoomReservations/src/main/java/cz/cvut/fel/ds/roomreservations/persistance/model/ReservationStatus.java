/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ds.roomreservations.persistance.model;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public enum ReservationStatus {

    UNKNOWN, DECLINED, CANCELLED, PENDING, ACCEPTED, COMPLETED;
}
