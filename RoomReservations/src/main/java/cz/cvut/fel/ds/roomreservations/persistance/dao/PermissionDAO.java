/*
 * Copyright (C) 2014 Ladislav Mejzlík <lmejzlik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao;

import cz.cvut.fel.ds.roomreservations.persistance.model.Permission;
import java.util.List;

/**
 * Data Acces Object for Permission entity.
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public interface PermissionDAO {

    /**
     * Retrives permission with specified id.
     *
     * @param permissionId Desired id
     * @return desired permission or null if permission with specified id does not exist
     */
    Permission getPermissionByID(Integer permissionId);

    /**
     * Retrives permission with specified name.
     *
     * @param name Desired permission name
     * @return desired permission or null if permission with specified name does
     * not exist
     */
    Permission getPermissionByName(String name);

    /**
     * Adds permission to the database. Permission id can be set to specific value, or
     * null to generate new id, generated id is set to the object.
     *
     * @param permission
     * @return true if permission was successfully added
     */
    boolean addPermission(Permission permission);

    /**
     * Updates permission in database.
     *
     * @param permission Permission to update, with id different form null
     * @return true if permission was successfully updated
     */
    boolean updatePermission(Permission permission);

    /**
     * Deletes permission from database.
     *
     * @param permissionId Id of permission to delete, can't be null
     * @return true if permission was successfully deleted
     */
    boolean deletePermission(Integer permissionId);

    /**
     * Retrieves all permissions from database.
     *
     * @return list of permissions
     */
    List<Permission> getAllPermissions();

}
