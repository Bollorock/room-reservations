/*
 * Copyright (C) 2015 Ladislav Mejzlík <lmejzlik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.persistance.dao.PriceModifierDAO;
import cz.cvut.fel.ds.roomreservations.persistance.model.PriceModifer;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class PriceModifierDAOImpl extends CommonDAOImpl<PriceModifer> implements PriceModifierDAO {

    private static final Logger LOG = Logger.getLogger(PriceModifierDAOImpl.class.getName());

    public PriceModifierDAOImpl() {
        super(PriceModifer.class);
    }

    @Override
    public PriceModifer getPriceModifier(int dayOfWeek, int timeWindowId, int roomId) {
        Transaction tx = null;
        PriceModifer mod = null;
        try {
            Session localSession = getSession();
            tx = localSession.beginTransaction();
            Criteria cr = localSession.createCriteria(PriceModifer.class);
            cr.add(
                    Restrictions.and(
                            Restrictions.eq("dayOfWeek", dayOfWeek),
                            Restrictions.eq("timeWindow.id", timeWindowId),
                            Restrictions.eq("room.id", roomId)));
            cr.setMaxResults(1);
            mod = (PriceModifer) (cr.uniqueResult());
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.log(Level.SEVERE, null, e);
        }
        return mod;
    }

}
