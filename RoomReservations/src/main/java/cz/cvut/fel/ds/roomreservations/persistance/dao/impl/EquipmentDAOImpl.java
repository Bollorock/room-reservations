/*
 * Copyright (C) 2015 Ond?ej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.persistance.dao.EquipmentDAO;
import cz.cvut.fel.ds.roomreservations.persistance.model.Equipment;
import java.util.List;

public class EquipmentDAOImpl extends CommonDAOImpl<Equipment> implements EquipmentDAO {

    public EquipmentDAOImpl() {
        super(Equipment.class);
    }

    @Override
    public Equipment getEquipmentByID(Integer equipmentId) {
        return getEntityById(equipmentId);
    }

    @Override
    public List<Equipment> getEquipmentsByRoom(Integer roomId) {
        return getAllEntityByColumn("room.id", roomId);
    }

    @Override
    public boolean addEquipment(Equipment equipment) {
        return addEntity(equipment);
    }

    @Override
    public boolean updateEquipment(Equipment equipment) {
        return updateEntity(equipment);
    }

    @Override
    public boolean deleteEquipment(Integer equipmentId) {
        return deleteEntity(equipmentId);
    }

    @Override
    public List<Equipment> getAllEquipments() {
        return getAllEntities();
    }

}
