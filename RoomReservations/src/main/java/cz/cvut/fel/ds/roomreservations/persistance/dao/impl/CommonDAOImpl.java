/*
 * Copyright (C) 2014 Ladislav Mejzlík <lmejzlik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.util.HibernateUtil;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Entity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 * Class with common methods of DAOs
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 * @param <T>
 */
public class CommonDAOImpl<T extends DataClass> {

    private static final Logger LOG = Logger.getLogger(CommonDAOImpl.class.getName());

    private Session session;
    private Class<T> clazz;
    private String entityName;

    protected CommonDAOImpl(Class<T> clazz) {
        this(clazz, null);
    }

    protected CommonDAOImpl(Class<T> clazz, Session session) {

        this.clazz = clazz;
        this.session = session;

        entityName = clazz.getAnnotation(Entity.class).name();
    }

    protected Session getSession() {
        if (session == null || !session.isOpen()) {
            session = HibernateUtil.getSessionFactory().openSession();
        }
        return session;
    }

    /**
     * Retrives entity with specified id.
     *
     * @param id Desired id
     * @return desired entity or null if entity with specified id does not exist
     */
    protected T getEntityById(Integer id) {
        if (id == null) {
            return null;
        }
        try {
            T entity = (T) getSession().get(clazz, id);
            LOG.log(Level.INFO, "Got {0}: (#{1}) {2}", new Object[]{entityName, entity == null ? "" : entity.getId(), entity});
            return entity;
        } catch (HibernateException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return null;
    }

    /**
     * Adds entity to the database. Entity id can be set to specific value, or
     * null to generate new id, generated id is set to the object.
     *
     * @param entity
     * @return true if entity was successfully added
     */
    protected boolean addEntity(T entity) {
        if (entity == null) {
            return false;
        }
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            Integer id = (Integer) session.save(entity);
            tx.commit();
            entity.setId(id);
            LOG.log(Level.INFO, "Added {0}: (#{1}) {2}", new Object[]{entityName, entity.getId(), entity});
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.log(Level.SEVERE, null, e);
        } finally {
            getSession().close();
        }
        return false;
    }

    /**
     * Retrieves all entities from database.
     *
     * @return list of entities
     */
    protected List<T> getAllEntities() {
        List entities = null;
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            entities = getSession().createQuery("FROM " + entityName).list();
            tx.commit();
            LOG.log(Level.INFO, "Got all {1} {0}", new Object[]{entityName, entities.size()});
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.log(Level.SEVERE, null, e);
        }
        return (List<T>) (Object) entities;
    }

    /**
     * Updates entity in database.
     *
     * @param entity Entity to update, with id different form null
     * @return true if entity was successfully updated
     */
    protected boolean updateEntity(T entity) {
        if (entity == null || entity.getId() == null) {
            return false;
        }
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().merge(entity);
//            getSession().saveOrUpdate(entity);
            tx.commit();
            LOG.log(Level.INFO, "Updated {0}: (#{1}) {2}", new Object[]{entityName, entity.getId(), entity});
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.log(Level.SEVERE, null, e);
        }
        return false;
    }

    /**
     * Deletes entity from database.
     *
     * @param id Id of entity to delete, can't be null
     * @return true if entity was successfully deleted
     */
    protected boolean deleteEntity(Integer id) {
        if (id == null) {
            return false;
        }
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            T entity = (T) getSession().get(clazz, id);
            getSession().delete(entity);
            tx.commit();
            LOG.log(Level.INFO, "Deleted {0}: (#{1}) {2}", new Object[]{entityName, entity.getId(), entity});
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.log(Level.SEVERE, null, e);
        }
        return false;
    }

    /**
     * Retrieves first entity with specified value in specified column.
     *
     * @param columnName
     * @param value
     * @return entity
     */
    protected T getFirstEntityByColumn(String columnName, Object value) {
        Transaction tx = null;
        T entity = null;
        try {
            Session localSession = getSession();
            tx = localSession.beginTransaction();
            Criteria cr = localSession.createCriteria(clazz);
            cr.add(Restrictions.eq(columnName, value));
            cr.setMaxResults(1);
            entity = (T) (cr.uniqueResult());
            tx.commit();
            LOG.log(Level.INFO, "Got first {1} by {0}: (#{2}) {3}", new Object[]{columnName, entityName, entity == null ? "" : entity.getId(), entity});
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.log(Level.SEVERE, null, e);
        }
        return entity;
    }

    /**
     * Retrieves all entities with specified value in specified column.
     *
     * @param columnName
     * @param value
     * @return list of entities
     */
    protected List<T> getAllEntityByColumn(String columnName, Object value) {
        Transaction tx = null;
        List entities = null;
        try {
            Session localSession = getSession();
            tx = localSession.beginTransaction();
            Criteria cr = localSession.createCriteria(clazz);
            System.out.printf("%s (%s: %s)\n", entityName, columnName, value);
            cr.add(Restrictions.eq(columnName, value));
            entities = cr.list();
            tx.commit();
            LOG.log(Level.INFO, "Got all {2} {1} by {0}", new Object[]{columnName, entityName, entities.size()});
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.log(Level.SEVERE, null, e);
        }
        return (List<T>) (Object) entities;
    }
}
