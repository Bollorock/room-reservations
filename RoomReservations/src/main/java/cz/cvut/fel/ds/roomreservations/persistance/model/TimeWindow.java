/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ds.roomreservations.persistance.model;

import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.DataClass;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
@Entity(name = "TimeWindow")
public class TimeWindow implements DataClass, Serializable {

    private static final long serialVersionUID = -2956508971758367816L;

    @Id
    @Column(name = "timewindow_id", unique = true, nullable = false)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private int id;

    @Column(name = "from", nullable = false, unique = false)
    @Temporal(TemporalType.TIME)
    private Date timeFrom;

    @Column(name = "to", nullable = false, unique = false)
    @Temporal(TemporalType.TIME)
    private Date timeTo;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(Date timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Date getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(Date timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public String toString() {
        return "Hodina " + id + " (" + timeFrom + '-' + timeTo + ')';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TimeWindow other = (TimeWindow) obj;
        return this.id == other.id;
    }

}
