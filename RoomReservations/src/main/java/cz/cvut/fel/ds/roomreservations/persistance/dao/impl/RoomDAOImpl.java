/*
 * Copyright (C) 2015 Ond?ej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.persistance.dao.RoomDAO;
import cz.cvut.fel.ds.roomreservations.persistance.model.Room;
import java.util.List;

public class RoomDAOImpl extends CommonDAOImpl<Room> implements RoomDAO {

    public RoomDAOImpl() {
        super(Room.class);
    }

    @Override
    public Room getRoomByID(Integer roomId) {
        return getEntityById(roomId);
    }

    @Override
    public Room getRoomByName(String name) {
        return getFirstEntityByColumn("name", name);
    }

    @Override
    public boolean addRoom(Room room) {
        return addEntity(room);
    }

    @Override
    public boolean updateRoom(Room room) {
        return updateEntity(room);
    }

    @Override
    public boolean deleteRoom(Integer roomId) {
        return deleteEntity(roomId);
    }

    @Override
    public List<Room> getAllRooms() {
        return getAllEntities();
    }

}
