package cz.cvut.fel.ds.roomreservations.presentation.controller;

import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.PersonDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.model.Person;
import cz.cvut.fel.ds.roomreservations.presentation.ui.SessionContext;
import cz.cvut.fel.ds.roomreservations.util.PasswordHash;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class LoginController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    private static final PersonDAOImpl pdao = new PersonDAOImpl();

    /**
     * Method that tries logging into the program using provided creditentials.
     *
     * TODO: Something.
     *
     * @param userName
     * @param password
     * @return true for successful login, false for unsuccessful
     */
    public static boolean login(String userName, char[] password) {
        try {
            Person p = pdao.getPersonByUsername(userName);
            if (p == null) {
                return false;
            }
            boolean validatePassword = PasswordHash.validatePassword(password, p.getPassword());
            if (validatePassword) {
                SessionContext.setLoggedIn(p);
                return true;
            } else {
                return false;
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
