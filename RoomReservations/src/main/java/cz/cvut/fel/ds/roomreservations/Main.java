package cz.cvut.fel.ds.roomreservations;

import cz.cvut.fel.ds.roomreservations.persistance.dao.*;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.*;
import cz.cvut.fel.ds.roomreservations.persistance.model.Reservation;
import cz.cvut.fel.ds.roomreservations.persistance.model.ReservationStatus;
import cz.cvut.fel.ds.roomreservations.util.HibernateUtil;
import cz.cvut.fel.ds.roomreservations.util.PasswordHash;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    //Logger setup
    private static final Level LOG_LEVEL = Level.INFO;
    private static final String LOG_DIR = "log";
    private static final String LOG_NAME = String.format("%1$tF %1$tH-%1$tM-%1$tS.log", new Date());
    private static final int LOG_MAX_FILE_SIZE = 10 * 1024 * 1024; // 10MB
    private static final int LOG_MAX_FILES_COUNT = 10;
    private static final boolean LOG_APPEND = false;

    /**
     * Run different methods according to first argument.
     *
     * @param args
     */
    public static void main(String[] args) {
        loggerSetup();
        deleteOldLogs();

        if (args.length > 0 && ("-h".equals(args[0]) || "--hibernate".equals(args[0]))) {
            LOG.log(Level.INFO, "Executing hibernate path");
            hibernatePath();
        } else if (args.length > 0 && ("-g".equals(args[0]) || "--guiTest".equals(args[0]))) {
            LOG.log(Level.INFO, "Executing gui test");
            guiTestPath();
        } else if (args.length > 0 && ("-p".equals(args[0]) || "--passGen".equals(args[0]))) {
            LOG.log(Level.INFO, "Executing password hash generator");
            generatePasswordHashPath();
        } else {
            LOG.log(Level.INFO, "Executing default path");
            defaultPath();
        }
    }

    /**
     * Default path for end user.
     */
    private static void defaultPath() {
        try {
            System.out.println(PasswordHash.createHash("heslo"));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Path for Gui test
     */
    private static void guiTestPath() {
    }

    private static void generatePasswordHashPath() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Enter password:");
            String pass = sc.nextLine();
            try {
                System.out.println(PasswordHash.createHash(pass));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                Logger.getLogger(PasswordHash.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Tests hibernate connection to database.
     */
    private static void hibernatePath() {
        try {
            HibernateUtil.getSessionFactory();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
            return;
        }
        ReservationDAO reservationDAO = new ReservationDAOImpl();
        TimeWindowDAO timeWindowDAO = new TimeWindowDAOImpl();
        PersonDAO personDAO = new PersonDAOImpl();
        RoomDAO roomDAO = new RoomDAOImpl();
        Reservation r = new Reservation();
        r.setDate(Calendar.getInstance().getTime());
        r.setNote("Testovací rezervace.");
        r.setPerson(personDAO.getPersonByUsername("kacer"));
        r.setRoom(roomDAO.getRoomByName("AB-1"));
        r.setStatus(ReservationStatus.PENDING);
        r.setTimeWindows(new HashSet<>(timeWindowDAO.getAllTimeWindows()));
        System.out.println(r);
        System.out.println(reservationDAO.addReservation(r));
        System.out.println(reservationDAO.getAllReservations());
        HibernateUtil.shutdown();
    }

    private static void loggerSetup() throws SecurityException {
        File logDir = new File(LOG_DIR);
        logDir.mkdirs();
        Logger.getLogger("").setLevel(LOG_LEVEL);
        try {
            Handler handler = new FileHandler(LOG_DIR + "/" + LOG_NAME, LOG_MAX_FILE_SIZE, 1, LOG_APPEND);
            Logger.getLogger("").addHandler(handler);
        } catch (IOException | SecurityException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        for (Handler handler : Logger.getLogger("").getHandlers()) {
            handler.setLevel(LOG_LEVEL);
        }
    }

    private static void deleteOldLogs() {
        File logDir = new File(LOG_DIR);
        if (logDir.canRead() && logDir.isDirectory()) {
            File[] logs = logDir.listFiles((File dir, String name) -> name.matches(".*\\.log$"));
            if (logs.length >= LOG_MAX_FILES_COUNT) {
                Arrays.sort(logs, 0, logs.length,
                        (File o1, File o2) -> (int) Math.signum(o1.lastModified() - o2.lastModified()));
                for (int i = 0; i < logs.length - LOG_MAX_FILES_COUNT; i++) {
                    LOG.log(Level.FINE, "Deleting old log {0}", logs[i]);
                    logs[i].delete();
                }
            }
        }
    }
}
