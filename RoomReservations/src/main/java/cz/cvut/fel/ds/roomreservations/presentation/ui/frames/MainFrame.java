package cz.cvut.fel.ds.roomreservations.presentation.ui.frames;

import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.util.HibernateUtil;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class MainFrame extends JFrame {
    private static final long serialVersionUID = -199213098820237799L;

    public MainFrame() {

        this.setResizable(false);
        this.setSize(Values.WINDOW_HEIGHT, Values.WINDOW_WIDTH);
        initialize();
        this.setLocationRelativeTo(null);
        setVisible(true);
    }
    
    @Override
    public void pack(){
        super.pack();
    }

    private void initialize() {
        PanelSwitcher.setMainFrame(this);

        PanelSwitcher.switchPanel(Values.PANEL_LOGIN);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(Values.WINDOW_WIDTH, Values.WINDOW_HEIGHT));

        setTitle("Room Reservation");
        
    }

    public static void main(String args[]) {
        new Thread(HibernateUtil::getSessionFactory).start();

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainFrame();
        });
    }

}
