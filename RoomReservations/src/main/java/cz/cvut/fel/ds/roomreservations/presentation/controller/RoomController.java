/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.presentation.controller;

import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.RoomDAOImpl;
import cz.cvut.fel.ds.roomreservations.persistance.model.Room;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class RoomController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    private static final RoomDAOImpl rdao = new RoomDAOImpl();

    
    public static Room getRoomByName(String name){
        
        return rdao.getRoomByName(name);
    }
    
    public static List<Room> getAllRooms(){
        return rdao.getAllRooms();
    }
    
    public static Object[][] getRowData(List<Room> allRooms){        
        Object rowData[][] = new Object[allRooms.size()][5];
        for (int i = 0; i < allRooms.size(); i++) {
            rowData[i][0] = allRooms.get(i).getId();
            rowData[i][1] = allRooms.get(i).getName();
            rowData[i][2] = allRooms.get(i).getCapacity();
            rowData[i][3] = allRooms.get(i).isAvailable();
            rowData[i][4] = allRooms.get(i).getBasePrice();
        }
        return rowData;
    }
}
