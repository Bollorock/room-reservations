/*
 * Copyright (C) 2014 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao;

import cz.cvut.fel.ds.roomreservations.persistance.model.Role;
import java.util.List;

/**
 * Data Access Object for Role entity.
 * 
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public interface RoleDAO {

    /**
     * Retrives role with specified id.
     *
     * @param roleId Desired id
     * @return desired role or null if role with specified id does not exist
     */
    Role getRoleByID(Integer roleId);

    /**
     * Retrives role with specified rolename.
     *
     * @param name Desired role name
     * @return desired role or null if role with specified rolename does
     * not exist
     */
    Role getRoleByName(String name);

    /**
     * Adds role to the database. Role id can be set to specific value, or
     * null to generate new id, generated id is set to the object.
     *
     * @param role
     * @return true if role was successfully added
     */
    boolean addRole(Role role);

    /**
     * Updates role in database.
     *
     * @param role Role to update, with id different form null
     * @return true if role was successfully updated
     */
    boolean updateRole(Role role);

    /**
     * Deletes role from database.
     *
     * @param roleId Id of role to delete, can't be null
     * @return true if role was successfully deleted
     */
    boolean deleteRole(Integer roleId);

    /**
     * Retrieves all roles from database.
     *
     * @return list of roles
     */
    List<Role> getAllRoles();

}
