/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.presentation.ui.panels;

import cz.cvut.fel.ds.roomreservations.presentation.controller.ReserveController;
import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.presentation.ui.SessionContext;
import cz.cvut.fel.ds.roomreservations.presentation.ui.dialogs.ReserveEditDialog;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class ReserveOwnPanel extends JPanel {
    private static final long serialVersionUID = 7033515800185324712L;

    private JScrollPane panelTable;
    private JTable table;
    private JPanel panelButtons;

    private JButton buttonCancel;
    private JButton buttonEdit;
    private JButton buttonBack;

    private Object[][] rowData;

    public ReserveOwnPanel() {

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        initComponent();
        this.setVisible(true);
    }

    private void initComponent() {
        removeAll();
        panelButtons = new JPanel(new FlowLayout());
        buttonCancel = new JButton("Withdraw reservation");
        buttonEdit = new JButton("Edit note");
        buttonBack = new JButton("Back");

        panelButtons.add(buttonCancel);
        panelButtons.add(buttonEdit);
        panelButtons.add(buttonBack);

        initTable();

        add(panelTable);
        add(panelButtons);
        addActionListeners();
    }

    private void initTable() {
        rowData = ReserveController.getRowData(ReserveController.getUserReservations(SessionContext.getLoggedIn()));
        Object columns[] = {"reservation_id", "date", "note", "status", "room_id"};

        table = new JTable(rowData, columns);

        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        panelTable = new JScrollPane(table);
    }

    private void addActionListeners() {
        buttonCancel.addActionListener((ActionEvent ae) -> {
            int selectedRow = table.getSelectedRow();
            int selectedID = (int) rowData[selectedRow][0];
            ReserveController.cancelReservation(selectedID);
            initComponent();
            repaint();
            revalidate();
        });

        buttonEdit.addActionListener((ActionEvent ae) -> {
            int selectedRow = table.getSelectedRow();
            int selectedID = (int) rowData[selectedRow][0];
            ReserveEditDialog reserveEditFrame = new ReserveEditDialog(ReserveController.getReservationByID(selectedID), false);
            initComponent();
            repaint();
            revalidate();
        });

        buttonBack.addActionListener((ActionEvent ae) -> {
            PanelSwitcher.switchPanel(Values.PANEL_MAIN);
        });
    }

}
