package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.persistance.dao.PersonDAO;
import cz.cvut.fel.ds.roomreservations.persistance.model.Person;
import java.util.List;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class PersonDAOImpl extends CommonDAOImpl<Person> implements PersonDAO {

    public PersonDAOImpl() {
        super(Person.class);
    }

    @Override
    public Person getPersonByID(Integer personId) {
        return getEntityById(personId);
    }

    @Override
    public Person getPersonByUsername(String username) {
        return getFirstEntityByColumn("username", username);
    }

    @Override
    public boolean addPerson(Person person) {
        return addEntity(person);
    }

    @Override
    public boolean updatePerson(Person person) {
        return updateEntity(person);
    }

    @Override
    public boolean deletePerson(Integer personId) {
        return deleteEntity(personId);
    }

    @Override
    public List<Person> getAllPersons() {
        return getAllEntities();
    }
}
