package cz.cvut.fel.ds.roomreservations.presentation.ui.panels;

import cz.cvut.fel.ds.roomreservations.persistance.model.Permission;
import cz.cvut.fel.ds.roomreservations.persistance.model.Role;
import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.presentation.ui.SessionContext;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class MainPanel extends JPanel {
    private static final long serialVersionUID = 1489635211334249197L;

    private JButton button_createReservation;
    private JButton button_registerPerson;
    private JButton button_manageRooms;
    private JButton button_ownReservation;
    private JButton button_editReservation;

    private JButton buttonBack;

    public MainPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        initComponent();
        this.setVisible(true);

    }

    private void initComponent() {
        removeAll();
        button_createReservation = new JButton("Make a new reservation");
        button_registerPerson = new JButton("Register new person");
        button_manageRooms = new JButton("Manage Rooms");
        button_ownReservation = new JButton("Show my own reservations.");
        button_editReservation = new JButton("Edit reservations");

        buttonBack = new JButton("Logout");

        add(button_ownReservation);

        for (Role r : SessionContext.getLoggedInRoles()) {
            for (Permission per : r.getPermissions()) {
                switch (per.getName()) {
                    case Values.PERMISSION_RESERVE_CREATE:
                        add(button_createReservation);
                        break;
                    case Values.PERMISSION_USER_REGISTER:
                        add(button_registerPerson);
                        break;
                    case Values.PERMISSION_ROOM_MANAGE:
                        add(button_manageRooms);
                        break;
                    case Values.PERMISSION_RESERVE_EDIT:
                        add(button_editReservation);
                        break;
                    default:
                        break;
                }
            }
        }
        add(buttonBack);

        addActionListeners();
    }

    private void addActionListeners() {
        button_createReservation.addActionListener((ActionEvent e) -> {
            PanelSwitcher.switchPanel(Values.PANEL_RESERVE_CREATE);
        });
        button_registerPerson.addActionListener((ActionEvent e) -> {
            PanelSwitcher.switchPanel(Values.PANEL_REGISTER);
        });

        button_manageRooms.addActionListener((ActionEvent e) -> {
            PanelSwitcher.switchPanel(Values.PANEL_ROOMS);
        });

        buttonBack.addActionListener((ActionEvent e) -> {
            SessionContext.setLoggedIn(null);
            PanelSwitcher.switchPanel(Values.PANEL_LOGIN);
        });

        button_ownReservation.addActionListener((ActionEvent e) -> {
            PanelSwitcher.switchPanel(Values.PANEL_RESERVE_OWN);
        });

        button_editReservation.addActionListener((ActionEvent e) -> {
            PanelSwitcher.switchPanel(Values.PANEL_RESERVE_MANAGE);
        });
    }
}
