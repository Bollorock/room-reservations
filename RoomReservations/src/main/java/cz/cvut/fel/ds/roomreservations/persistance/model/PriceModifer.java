/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ds.roomreservations.persistance.model;

import com.google.gson.GsonBuilder;
import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.DataClass;
import java.io.Serializable;
import java.time.DayOfWeek;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
@Entity(name = "PriceModifier")
public class PriceModifer implements Serializable, DataClass {

    private static final long serialVersionUID = -1025215410418546555L;

    @Id
    @Column(name = "price_modifier_id", unique = true, nullable = false)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(name = "day_of_week", nullable = false)
    private int dayOfWeek;

    @Column(name = "price_modifier")
    private float priceModifier;

    @ManyToOne(optional = false)
    @JoinColumn(name = "timewindow_id")
    private TimeWindow timeWindow;

    @ManyToOne(optional = false)
    @JoinColumn(name = "room_id")
    private Room room;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public float getPriceModifier() {
        return priceModifier;
    }

    public void setPriceModifier(float priceModifier) {
        this.priceModifier = priceModifier;
    }

    public TimeWindow getTimeWindow() {
        return timeWindow;
    }

    public void setTimeWindow(TimeWindow timeWindow) {
        this.timeWindow = timeWindow;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

}
