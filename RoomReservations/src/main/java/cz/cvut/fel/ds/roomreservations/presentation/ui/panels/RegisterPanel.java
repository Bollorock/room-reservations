/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ds.roomreservations.presentation.ui.panels;

import cz.cvut.fel.ds.roomreservations.presentation.controller.RegisterController;
import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.util.GUIUtil;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class RegisterPanel extends JPanel {
    private static final long serialVersionUID = -4695671916353551776L;

    private JPanel panelFirstName;
    private JPanel panelLastName;
    private JPanel panelEmail;
    private JPanel panelUser;
    private JPanel panelPass;
    private JPanel panelPass2;

    private JPanel buttonPanel;

    private JButton buttonOK;
    private JButton buttonExit;

    private JLabel labelFirstName;
    private JLabel labelLastName;
    private JLabel labelEmail;
    private JLabel labelUser;
    private JLabel labelPass;
    private JLabel labelPass2;

    private JTextField textFieldFirstName;
    private JTextField textFieldLastName;
    private JTextField textFieldEmail;
    private JTextField textFieldUser;
    private JPasswordField textFieldPass;
    private JPasswordField textFieldPass2;

    public RegisterPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        initComponent();
        this.setVisible(true);
    }

    private void initComponent() {
        this.panelFirstName = new JPanel(new FlowLayout());
        this.panelLastName = new JPanel(new FlowLayout());
        this.panelEmail = new JPanel(new FlowLayout());
        this.panelUser = new JPanel(new FlowLayout());
        this.panelPass = new JPanel(new FlowLayout());
        this.panelPass2 = new JPanel(new FlowLayout());

        this.buttonPanel = new JPanel(new FlowLayout());

        buttonOK = new JButton("OK");
        buttonExit = new JButton("Back");

        labelFirstName = new JLabel("First Name:");
        labelLastName = new JLabel("Last Name:");
        labelEmail = new JLabel("Email:");
        labelUser = new JLabel("Username:");
        labelPass = new JLabel("Password:");
        labelPass2 = new JLabel("Confirm Password:");

        textFieldFirstName = new JTextField();
        textFieldFirstName.setPreferredSize(new Dimension(200, 30));
        textFieldFirstName.setHorizontalAlignment(JTextField.CENTER);

        textFieldLastName = new JTextField();
        textFieldLastName.setPreferredSize(new Dimension(200, 30));
        textFieldLastName.setHorizontalAlignment(JTextField.CENTER);

        textFieldEmail = new JTextField();
        textFieldEmail.setPreferredSize(new Dimension(200, 30));
        textFieldEmail.setHorizontalAlignment(JTextField.CENTER);

        textFieldUser = new JTextField();
        textFieldUser.setPreferredSize(new Dimension(200, 30));
        textFieldUser.setHorizontalAlignment(JTextField.CENTER);

        textFieldPass = new JPasswordField();
        textFieldPass.setPreferredSize(new Dimension(200, 30));
        textFieldPass.setHorizontalAlignment(JTextField.CENTER);
        textFieldPass.setEchoChar('*');

        textFieldPass2 = new JPasswordField();
        textFieldPass2.setPreferredSize(new Dimension(200, 30));
        textFieldPass2.setHorizontalAlignment(JTextField.CENTER);
        textFieldPass2.setEchoChar('*');

        panelFirstName.add(labelFirstName);
        panelFirstName.add(textFieldFirstName);
        panelLastName.add(labelLastName);
        panelLastName.add(textFieldLastName);
        panelEmail.add(labelEmail);
        panelEmail.add(textFieldEmail);
        panelUser.add(labelUser);
        panelUser.add(textFieldUser);
        panelPass.add(labelPass);
        panelPass.add(textFieldPass);
        panelPass2.add(labelPass2);
        panelPass2.add(textFieldPass2);

        buttonPanel.add(buttonOK);
        buttonPanel.add(buttonExit);

        addActionListeners();

        add(panelFirstName);
        add(panelLastName);
        add(panelEmail);
        add(panelUser);
        add(panelPass);
        add(panelPass2);

        add(buttonPanel);
    }

    private void addActionListeners() {
        buttonOK.addActionListener((ActionEvent ae) -> {
            char[] pass1 = textFieldPass.getPassword();
            char[] pass2 = textFieldPass2.getPassword();
            if (!Arrays.equals(pass1, pass2)) {
                GUIUtil.displayError("Passwords don't match.");
                textFieldPass.setText("");
                textFieldPass2.setText("");
                return;
            }
            boolean register = RegisterController.register(textFieldFirstName.getText(),
                    textFieldLastName.getText(),
                    textFieldEmail.getText(),
                    textFieldUser.getText(),
                    pass1);
            if (!register) {
                GUIUtil.displayError("Something went wrong.\r\n"
                        + "Username might be taken or you entered invalid email.");
                textFieldPass.setText("");
                textFieldPass2.setText("");
            } else {
                GUIUtil.displayOK("Person registered successfully");
                PanelSwitcher.switchPanel(Values.PANEL_MAIN);
            }
        }
        );

        buttonExit.addActionListener(
                (ActionEvent ae) -> {
                    PanelSwitcher.switchPanel(Values.PANEL_MAIN);
                }
        );
    }

}
