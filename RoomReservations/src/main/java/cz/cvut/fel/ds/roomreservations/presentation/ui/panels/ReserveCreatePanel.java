/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.presentation.ui.panels;

import cz.cvut.fel.ds.roomreservations.persistance.model.Room;
import cz.cvut.fel.ds.roomreservations.persistance.model.TimeWindow;
import cz.cvut.fel.ds.roomreservations.presentation.controller.ReserveController;
import cz.cvut.fel.ds.roomreservations.presentation.controller.RoomController;
import cz.cvut.fel.ds.roomreservations.presentation.ui.PanelSwitcher;
import cz.cvut.fel.ds.roomreservations.presentation.ui.SessionContext;
import cz.cvut.fel.ds.roomreservations.util.GUIUtil;
import cz.cvut.fel.ds.roomreservations.util.JTextFieldLimit;
import cz.cvut.fel.ds.roomreservations.util.Values;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.jdatepicker.JDateComponentFactory;
import org.jdatepicker.JDatePicker;
import org.joda.time.DateTime;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class ReserveCreatePanel extends JPanel {
    private static final long serialVersionUID = 2736793261282388930L;

    private JPanel panelRoom;
    private JPanel panelDate;

    private JPanel panelNote;

    private JPanel panelCheckboxes;
    private JPanel panelPrices;

    private JPanel panelButtons;

    private JComboBox roomcombo;
    private JDatePicker datePicker;
    private JTextArea textFieldNote;
    private JCheckBox[] checkboxes;

    private JTextField textPriceAvailable;
    private JTextField textPriceActual;

    private JButton buttonOK;
    private JButton buttonBack;

    private String selectedRoom;
    private int price;

    List<TimeWindow> allTWs;

    private GregorianCalendar selectedDate;

    public ReserveCreatePanel() {

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        initComponent();
        this.setVisible(true);
    }

    private void initComponent() {
        panelRoom = new JPanel(new FlowLayout());
        panelDate = new JPanel(new FlowLayout());
        panelNote = new JPanel(new FlowLayout());
        panelButtons = new JPanel(new FlowLayout());
        panelCheckboxes = new JPanel(new FlowLayout());
        panelPrices = new JPanel(new FlowLayout());

        List<Room> allRooms = RoomController.getAllRooms();
        String[] strings = allRooms.stream()
                .filter(sc -> sc.isAvailable())
                .map(sc -> sc.getName())
                .toArray(String[]::new);

        roomcombo = new JComboBox(strings);

        panelRoom.add(new JLabel("Room:"));
        panelRoom.add(roomcombo);

        JDateComponentFactory compFactory = new JDateComponentFactory();
        datePicker = compFactory.createJDatePicker();

        panelDate.add(new JLabel("Date:"));
        panelDate.add((Component) datePicker);

        textFieldNote = new JTextArea(4, 30);
        textFieldNote.setLineWrap(true);
        textFieldNote.setPreferredSize(textFieldNote.getSize());
        textFieldNote.setDocument(new JTextFieldLimit(120));

        panelNote.add(new JLabel("Note:"));
        panelNote.add(textFieldNote);

        allTWs = ReserveController.getAllTimeWindows();
        checkboxes = new JCheckBox[allTWs.size()];
        for (int i = 0; i < allTWs.size(); i++) {
            StringBuilder sb = new StringBuilder();
            sb.append((new DateTime(allTWs.get(i).getTimeFrom()).toString("HH:mm")));
            sb.append(" - ");
            sb.append((new DateTime(allTWs.get(i).getTimeTo()).toString("HH:mm")));
            checkboxes[i] = new JCheckBox(sb.toString());
            //TODO display text
            panelCheckboxes.add(checkboxes[i]);
            checkboxes[i].setEnabled(false);

        }
        selectedRoom = (String) roomcombo.getSelectedItem();
        selectedDate = (GregorianCalendar) datePicker.getModel().getValue();
        updateTimes();

        JPanel priceAvailable = new JPanel(new FlowLayout());

        textPriceAvailable = new JTextField(5);
        textPriceAvailable.setText(Integer.toString(SessionContext.getLoggedIn().getCredits()));
        textPriceAvailable.setEditable(false);
        priceAvailable.add(new JLabel("Available credits:"));
        priceAvailable.add(textPriceAvailable);

        JPanel priceActual = new JPanel(new FlowLayout());

        priceActual.add(new JLabel("Reservation cost:"));
        textPriceActual = new JTextField(5);
        textPriceActual.setText("0");
        textPriceActual.setEditable(false);
        priceActual.add(textPriceActual);

        panelPrices.add(priceAvailable);
        panelPrices.add(priceActual);

        buttonOK = new JButton("Reserve");
        buttonBack = new JButton("Back");

        panelButtons.add(buttonOK);
        panelButtons.add(buttonBack);

        add(panelRoom);
        add(panelDate);

        add(panelNote);
        add(panelCheckboxes);
        add(panelPrices);
        add(panelButtons);
        addActionListeners();
    }

    private void addActionListeners() {
        roomcombo.addActionListener((ActionEvent ae) -> {
            JComboBox cb = (JComboBox) ae.getSource();
            selectedRoom = (String) cb.getSelectedItem();
            updateTimes();
            updatePrice();
        });

        datePicker.addActionListener((ActionEvent ae) -> {
            selectedDate = (GregorianCalendar) datePicker.getModel().getValue();

            updateTimes();
            updatePrice();
        });

        buttonOK.addActionListener((ActionEvent ae) -> {
            Set<TimeWindow> twset = new HashSet<>();

            for (int i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].isSelected()) {
                    twset.add(allTWs.get(i));
                }
            }
            boolean add = ReserveController.makeReservation(selectedDate.getTime(), twset, RoomController.getRoomByName(selectedRoom), textFieldNote.getText(), price);
            if (!add) {
                GUIUtil.displayError("Something went wrong.");
            } else {
                GUIUtil.displayOK("Reservation placed.");
                PanelSwitcher.switchPanel(Values.PANEL_MAIN);
            }
        });

        for (JCheckBox ch : checkboxes) {
            ch.addActionListener((ActionEvent ae) -> {
                updatePrice();
            });
        }

        buttonBack.addActionListener((ActionEvent ae) -> {
            PanelSwitcher.switchPanel(Values.PANEL_MAIN);
        });

    }

    private void updateTimes() {
        List<TimeWindow> freeTimeWindows = ReserveController.getFreeTimeWindows(selectedDate.getTime(), RoomController.getRoomByName(selectedRoom));
        for (JCheckBox chb : checkboxes) {
            chb.setEnabled(false);
            chb.setSelected(false);
        }
        for (TimeWindow free : freeTimeWindows) {
            checkboxes[free.getId() - 1].setEnabled(true);
        }
        panelCheckboxes.repaint();
    }

    private void updatePrice() {       
        Set<TimeWindow> twset = new HashSet<>();

        for (int i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].isSelected()) {
                twset.add(allTWs.get(i));
            }
        }
        
        price = ReserveController.getReservationPrice(selectedDate.getTime(), twset, RoomController.getRoomByName(selectedRoom));
        
        textPriceActual.setText(Integer.toString(price));
        textPriceActual.repaint();
    }
}
