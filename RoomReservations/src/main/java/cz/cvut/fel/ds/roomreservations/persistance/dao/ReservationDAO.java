/*
 * Copyright (C) 2014 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao;

import cz.cvut.fel.ds.roomreservations.persistance.model.Reservation;
import java.util.List;

/**
 * Data Access Object for Reservation entity.
 * 
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public interface ReservationDAO {

    /**
     * Retrives reservation with specified id.
     *
     * @param reservationId Desired id
     * @return desired reservation or null if reservation with specified id does not exist
     */
    Reservation getReservationByID(Integer reservationId);

    /**
     * Retrives reservation with specified reservationname.
     *
     * @param personId
     * @return desired reservation or null if reservation with specified reservationname does
     * not exist
     */
    List<Reservation> getReservationsByUser(Integer personId);

    List<Reservation> getReservationsByRoom(Integer roomId);
    /**
     * Adds reservation to the database. Reservation id can be set to specific value, or
     * null to generate new id, generated id is set to the object.
     *
     * @param reservation
     * @return true if reservation was successfully added
     */
    boolean addReservation(Reservation reservation);

    /**
     * Updates reservation in database.
     *
     * @param reservation Reservation to update, with id different form null
     * @return true if reservation was successfully updated
     */
    boolean updateReservation(Reservation reservation);

    /**
     * Deletes reservation from database.
     *
     * @param reservationId Id of reservation to delete, can't be null
     * @return true if reservation was successfully deleted
     */
    boolean deleteReservation(Integer reservationId);

    /**
     * Retrieves all reservations from database.
     *
     * @return list of reservations
     */
    List<Reservation> getAllReservations();

}
