/*
 * Copyright (C) 2015 Ondřej Netík <ondrej.netik@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.ds.roomreservations.persistance.dao.impl;

import cz.cvut.fel.ds.roomreservations.persistance.dao.RoleDAO;
import cz.cvut.fel.ds.roomreservations.persistance.model.Role;
import java.util.List;

/**
 *
 * @author Ondřej Netík <ondrej.netik@gmail.com>
 */
public class RoleDAOImpl extends CommonDAOImpl<Role> implements RoleDAO {

    public RoleDAOImpl() {
        super(Role.class);
    }

    @Override
    public Role getRoleByID(Integer roleId) {
        return getEntityById(roleId);
    }

    @Override
    public Role getRoleByName(String name) {
        return getFirstEntityByColumn("name", name);
    }

    @Override
    public boolean addRole(Role role) {
        return addEntity(role);
    }

    @Override
    public boolean updateRole(Role role) {
        return updateEntity(role);
    }

    @Override
    public boolean deleteRole(Integer roleId) {
        return deleteEntity(roleId);
    }

    @Override
    public List<Role> getAllRoles() {
        return getAllEntities();
    }

}
