/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ds.roomreservations.persistance.model;

import cz.cvut.fel.ds.roomreservations.persistance.dao.impl.DataClass;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
@Entity(name = "EquipmentType")
public class EquipmentType implements DataClass, Serializable {

    private static final long serialVersionUID = 4941888867708418257L;
    
    @Id
    @Column(name = "equipment_type_id", unique = true, nullable = false)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "added_credit_value", nullable = false)
    private int addedCreditValue = 0;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAddedCreditValue() {
        return addedCreditValue;
    }

    public void setAddedCreditValue(int addedCreditValue) {
        this.addedCreditValue = addedCreditValue;
    }

}
