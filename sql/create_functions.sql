DROP FUNCTION IF EXISTS grant_admin_access(); 
DROP FUNCTION IF EXISTS get_user_id(varchar(64));
DROP FUNCTION IF EXISTS get_role_id(varchar(64));
DROP FUNCTION IF EXISTS get_room_id(varchar(64));
DROP FUNCTION IF EXISTS get_equipment_type_id(varchar(64));


--creates function to grant all permissions to user role admin
CREATE FUNCTION grant_admin_access() RETURNS integer AS $$
DECLARE
    result_id int4;
BEGIN
    FOR result_id IN (SELECT (permission_id) FROM permission) LOOP
        INSERT INTO role_permission VALUES (
		(SELECT role_id FROM role r WHERE r.name = 'admin'),
		result_id
		);

     END LOOP;
     RETURN 1;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION get_user_id(username varchar(64)) RETURNS integer AS $$
BEGIN
    RETURN (SELECT person_id FROM person p WHERE p.username = username LIMIT 1);
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION get_role_id(rolename varchar(64)) RETURNS integer AS $$
BEGIN
    return (SELECT role_id FROM role WHERE "name" = rolename LIMIT 1);
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION get_room_id(roomname varchar(64)) RETURNS integer AS $$
BEGIN
    return (SELECT room_id FROM room WHERE "room_name" = roomname LIMIT 1);
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION get_equipment_type_id(name varchar(64)) RETURNS integer AS $$
BEGIN
    return (SELECT equipment_type_id FROM equipmenttype WHERE "name" = name LIMIT 1);
END;
$$ LANGUAGE plpgsql;