----------------------------------------
-- get person wtih most spent credits
----------------------------------------
SELECT person.username, sum(reservation.cost) AS total_spent FROM person
    INNER JOIN reservation ON person.person_id = reservation.person_id AND reservation.status != 'CANCELLED'
    GROUP BY person.username
    ORDER BY total_spent DESC
    LIMIT 1;
    
----------------------------------------
-- return past demand for rooms
----------------------------------------
SELECT room.room_id, room.room_name, timewindow.timewindow_id, timewindow."from", timewindow."to", "count" FROM (
        SELECT reservation.room_id, reservation_timewindow.timewindow_id, count(1) FROM reservation 
            FULL JOIN reservation_timewindow ON reservation.reservation_id=reservation_timewindow.reservation_id
            INNER JOIN timewindow ON reservation_timewindow.timewindow_id = timewindow.timewindow_id
            GROUP BY reservation.room_id, reservation_timewindow.timewindow_id
    ) AS room_demand
    INNER JOIN room ON room_demand.room_id=room.room_id
    INNER JOIN timewindow ON room_demand.timewindow_id=timewindow.timewindow_id; 
    
----------------------------------------
-- get most reserved room
----------------------------------------
SELECT room.room_id, room.room_name, sum("count") as reserved_timewindows_count FROM (
        SELECT reservation.room_id, reservation_timewindow.timewindow_id, count(1) FROM reservation 
            FULL JOIN reservation_timewindow ON reservation.reservation_id=reservation_timewindow.reservation_id
            INNER JOIN timewindow ON reservation_timewindow.timewindow_id = timewindow.timewindow_id
            GROUP BY reservation.room_id, reservation_timewindow.timewindow_id
    ) AS room_demand
    INNER JOIN room ON room_demand.room_id=room.room_id
    INNER JOIN timewindow ON room_demand.timewindow_id=timewindow.timewindow_id
    GROUP BY room.room_id, room.room_name
    ORDER BY reserved_timewindows_count DESC
    LIMIT 1;
    