DELETE FROM role_permission;
DELETE FROM reservation_timewindow;
DELETE FROM person_role;
DELETE FROM equipment;
DELETE FROM pricemodifier;

DELETE FROM reservation;
DELETE FROM permission;
DELETE FROM role;
DELETE FROM person;
DELETE FROM timewindow;
DELETE FROM equipmenttype;
DELETE FROM room;

ALTER SEQUENCE permission_permission_id_seq RESTART WITH 1;
ALTER SEQUENCE role_role_id_seq RESTART WITH 1;
ALTER SEQUENCE person_person_id_seq RESTART WITH 1;
ALTER SEQUENCE timewindow_timewindow_id_seq RESTART WITH 1;
ALTER SEQUENCE equipmenttype_equipment_type_id_seq RESTART WITH 1;
ALTER SEQUENCE room_room_id_seq RESTART WITH 1;
ALTER SEQUENCE equipment_equipment_id_seq RESTART WITH 1;
ALTER SEQUENCE reservation_reservation_id_seq RESTART WITH 1;
ALTER SEQUENCE pricemodifier_price_modifier_id_seq RESTART WITH 1;

-- FUNCTIONS --


-- DATA --
INSERT INTO permission(name) VALUES
	('reserve_create'),
	('reserve_edit'),
	('room_manage'),
	('user_edit'),
  ('equipment_manage'),
  ('user_register'),
  ('roles_manage');

INSERT INTO role (name, description) VALUES
	('admin', 'Administrator'),
	('user', 'Common user'),
	('empty', 'No permissions');
  
-- ----------------------------
-- TIME WINDOW
-- ----------------------------
INSERT INTO timewindow ("from", "to") VALUES 
  ('07:30:00', '09:00:00'),
  ('09:15:00', '10:45:00'),
  ('11:00:00', '12:30:00'),
  ('12:45:00', '14:15:00'),
  ('14:30:00', '16:00:00'),
  ('16:15:00', '17:45:00'),
  ('18:00:00', '19:30:00');

INSERT INTO role_permission (role_id, permission_id) VALUES 
  (get_role_id('user'), (SELECT permission_id FROM permission WHERE name = 'reserve_create'));

SELECT grant_admin_access();

-- ----------------------------
-- PERSON
-- ----------------------------
--admin:admin
-- ostatní heslo "heslo"
INSERT INTO person(first_name,last_name,email,username,pwd,credits,weekly_income) VALUES
('Admin', 'Nejvyssi', 'admin@example.com', 'admin', '1000:ca060099cd0926f5adfdb98c94206083aa61a7df23c143b6:e00859bbceff219114e8e791567a90a5099bcdd172aece16',1000,100), 
('František', 'Novák', 'frnovak@example.com', 'frnov', '1000:5376f85206b30d2d71253b3b606fdfcebc742c4bf0e6a698:46ebdee18af275805f6af7d7d54554c37f6fc7833616aa23',1000,100), 
('Karolína', 'Červená', 'kacer@example.com', 'kacer', '1000:8627833056ff20a9f102d65812d721c95e20e499ff1976c3:e076bc2e77544e0abafb3cb29783b66098b12274c4a00e9a',1000,100), 
('Antonín', 'Kovář', 'antko@example.com', 'ankov', '1000:07b1f2a2cdb8a19b645765d2d7b1b587be7f428208c71745:ab4d6f3573a421665fe6049a63eb5ccc4a20f2826b0fda1a',1000,100), 
('Klaudie', 'Poledňáková', 'klapo@example.com', 'klpol', '1000:d36627e33949f4d92d481202a92d62c9fe0c4ce58a0dc42a:f75f4f02d13c4fb5ba1b0d8869e885a549cd2d2cb255404c',1000,100), 
('Alena', 'Vyskočilová', 'alvy@example.com', 'alvys', '1000:7a828551ba7fcf5e2a54ceacc1c6d6407c23d0d05fd86f71:5c711ba81fa5665ad042081669037ad3a065bc1db872e1e5',1000,100);

-- ----------------------------
-- PERSON -> ROLE
-- ----------------------------
INSERT INTO person_role VALUES
	(get_user_id('admin'),get_role_id('admin')),
	(get_user_id('frnov'),get_role_id('user')),
	(get_user_id('kacer'),get_role_id('user')),
	(get_user_id('ankov'),get_role_id('user')),
	(get_user_id('klpol'),get_role_id('user')),
	(get_user_id('alvys'),get_role_id('empty'));
  
-- ----------------------------
-- EQUIPMENT TYPE
-- ----------------------------
INSERT INTO equipmenttype (name, added_credit_value) VALUES 
  ('projektor', '20'),
  ('televize', '20'),
  ('meotar', '5'),
  ('klimatizace', '50');

-- ----------------------------
-- ROOMS
-- ----------------------------
INSERT INTO room (room_name, room_capacity, room_available, price_base) VALUES 
  ('AB-1', '20', 't', '10'),
  ('AB-2', '25', 't', '10'),
  ('BB-1', '150', 't', '60');

-- ----------------------------
-- EQUIPMENT
-- ----------------------------
INSERT INTO equipment (description, equipment_available, status, room_id, equipment_type_id) VALUES 
  ('Na stropě připevněný projektor s plátnem.', 't', 'Funkční', get_room_id('AB-1'), get_equipment_type_id('projektor')),
  ('Meotar.', 't', 'Funkční', get_room_id('AB-1'), get_equipment_type_id('meotar')),
  ('Plochá LCD televize.', 't', 'Funkční', get_room_id('AB-2'), get_equipment_type_id('televize')),
  ('Vysokovýnný projektor pro levou část místnosti.', 't', 'Funkční', get_room_id('BB-1'), get_equipment_type_id('projektor')),
  ('Vysokovýnný projektor pro pravou část místnosti.', 't', 'Funkční', get_room_id('BB-1'), get_equipment_type_id('projektor'));

-- ----------------------------
-- RESERVATIONS + RESERVATION_TIMEWINDOW
-- ----------------------------
INSERT INTO reservation ("date", note, status, person_id, room_id, cost) VALUES 
  ('2015-04-17', 'Test reservation', 'ACCEPTED', get_user_id('kacer'), get_room_id('BB-1'), 250);
INSERT INTO reservation_timewindow (reservation_id, timewindow_id) VALUES 
  (currval(pg_get_serial_sequence('reservation','reservation_id')), 3),  
  (currval(pg_get_serial_sequence('reservation','reservation_id')), 4);
  
INSERT INTO reservation ("date", note, status, person_id, room_id, cost) VALUES 
  ('2015-04-17', 'Test reservation', 'ACCEPTED', get_user_id('frnov'), get_room_id('BB-1'), 250);
INSERT INTO reservation_timewindow (reservation_id, timewindow_id) VALUES 
  (currval(pg_get_serial_sequence('reservation','reservation_id')), 5);
  
INSERT INTO reservation ("date", note, status, person_id, room_id, cost) VALUES 
  ('2015-04-17', 'Test reservation', 'ACCEPTED', get_user_id('alvys'), get_room_id('BB-1'), 250);
INSERT INTO reservation_timewindow (reservation_id, timewindow_id) VALUES 
  (currval(pg_get_serial_sequence('reservation','reservation_id')), 6);
  
INSERT INTO reservation ("date", note, status, person_id, room_id, cost) VALUES 
  ('2015-04-17', 'Test reservation', 'ACCEPTED', get_user_id('ankov'), get_room_id('AB-2'), 250);
INSERT INTO reservation_timewindow (reservation_id, timewindow_id) VALUES 
  (currval(pg_get_serial_sequence('reservation','reservation_id')), 2);
  
INSERT INTO reservation ("date", note, status, person_id, room_id, cost) VALUES 
  ('2015-04-18', 'Test reservation', 'ACCEPTED', get_user_id('ankov'), get_room_id('AB-2'), 250);
INSERT INTO reservation_timewindow (reservation_id, timewindow_id) VALUES 
  (currval(pg_get_serial_sequence('reservation','reservation_id')), 2);
  
INSERT INTO reservation ("date", note, status, person_id, room_id, cost) VALUES 
  ('2015-04-19', 'Test reservation', 'ACCEPTED', get_user_id('ankov'), get_room_id('AB-2'), 250);
INSERT INTO reservation_timewindow (reservation_id, timewindow_id) VALUES 
  (currval(pg_get_serial_sequence('reservation','reservation_id')), 2);

-- ----------------------------
-- PriceModif
-- ----------------------------
INSERT INTO pricemodifier (day_of_week, timewindow_id, room_id, price_modifier) VALUES 
  (6, 1, get_room_id('AB-1'), 0.8), 
  (6, 2, get_room_id('AB-1'), 0.8), 
  (6, 3, get_room_id('AB-1'), 0.8), 
  (6, 4, get_room_id('AB-1'), 0.8), 
  (6, 5, get_room_id('AB-1'), 0.8), 
  (6, 6, get_room_id('AB-1'), 0.8), 
  (6, 7, get_room_id('AB-1'), 0.8),  
  (7, 1, get_room_id('AB-1'), 0.8), 
  (7, 2, get_room_id('AB-1'), 0.8), 
  (7, 3, get_room_id('AB-1'), 0.8), 
  (7, 4, get_room_id('AB-1'), 0.8), 
  (7, 5, get_room_id('AB-1'), 0.8), 
  (7, 6, get_room_id('AB-1'), 0.8), 
  (7, 7, get_room_id('AB-1'), 0.8);
  
INSERT INTO pricemodifier (day_of_week, timewindow_id, room_id, price_modifier) VALUES 
  (6, 1, get_room_id('AB-2'), 0.8), 
  (6, 2, get_room_id('AB-2'), 0.8), 
  (6, 4, get_room_id('AB-2'), 0.8), 
  (6, 5, get_room_id('AB-2'), 0.8), 
  (6, 6, get_room_id('AB-2'), 0.8), 
  (6, 7, get_room_id('AB-2'), 0.8),  
  (7, 1, get_room_id('AB-2'), 0.8), 
  (7, 2, get_room_id('AB-2'), 0.8), 
  (7, 3, get_room_id('AB-2'), 0.8), 
  (7, 4, get_room_id('AB-2'), 0.8), 
  (7, 5, get_room_id('AB-2'), 0.8), 
  (7, 6, get_room_id('AB-2'), 0.8), 
  (7, 7, get_room_id('AB-2'), 0.8);
  
INSERT INTO pricemodifier (day_of_week, timewindow_id, room_id, price_modifier) VALUES 
  (6, 1, get_room_id('BB-1'), 0.8), 
  (6, 2, get_room_id('BB-1'), 0.8), 
  (6, 3, get_room_id('BB-1'), 0.8), 
  (6, 4, get_room_id('BB-1'), 0.8), 
  (6, 5, get_room_id('BB-1'), 0.8), 
  (6, 6, get_room_id('BB-1'), 0.8), 
  (6, 7, get_room_id('BB-1'), 0.8),  
  (7, 1, get_room_id('BB-1'), 0.8), 
  (7, 2, get_room_id('BB-1'), 0.8), 
  (7, 3, get_room_id('BB-1'), 0.8), 
  (7, 4, get_room_id('BB-1'), 0.8), 
  (7, 5, get_room_id('BB-1'), 0.8), 
  (7, 6, get_room_id('BB-1'), 0.8), 
  (7, 7, get_room_id('BB-1'), 0.8);