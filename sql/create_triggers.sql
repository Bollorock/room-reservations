CREATE OR REPLACE FUNCTION trg_charge_person() RETURNS TRIGGER AS $_$
BEGIN
  UPDATE person SET credits = credits - NEW.cost WHERE person_id = NEW.person_id;
  RETURN NEW;
END;  $_$ LANGUAGE 'plpgsql';  

CREATE OR REPLACE FUNCTION trg_chargeback_person() RETURNS TRIGGER AS $_$
BEGIN
  IF OLD.status!='CANCELLED' AND NEW.status ='CANCELLED' THEN
    UPDATE person SET credits = credits + NEW.cost WHERE person_id = NEW.person_id;
  END IF;	
  RETURN NEW;
END;  $_$ LANGUAGE 'plpgsql';


CREATE TRIGGER trg_charge_person BEFORE INSERT ON reservation FOR EACH ROW EXECUTE PROCEDURE trg_charge_person();
CREATE TRIGGER trg_chargeback_person BEFORE UPDATE ON reservation FOR EACH ROW EXECUTE PROCEDURE trg_chargeback_person();