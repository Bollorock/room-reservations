-- =============================================================================
-- Diagram Name: model1
-- Created on: 25. 3. 2015 17:40:25
-- Diagram Version: 
-- =============================================================================

--drop schema public cascade;
--create schema public;

DROP TABLE person CASCADE;
DROP TABLE role CASCADE;
DROP TABLE room CASCADE;
DROP TABLE reservation CASCADE;
DROP TABLE review CASCADE;
DROP TABLE equipment CASCADE;
DROP TABLE news CASCADE;
DROP TABLE equipmenttype CASCADE;
DROP TABLE post CASCADE;
DROP TABLE pricemodifier CASCADE;
DROP TABLE timewindow CASCADE;
DROP TABLE permission CASCADE;
DROP TABLE role_permission CASCADE;
DROP TABLE reservation_timewindow CASCADE;
DROP TABLE person_role CASCADE;

CREATE TABLE "person" (
	"person_id" SERIAL NOT NULL,
	"first_name" varchar(64) NOT NULL,
	"last_name" varchar(64) NOT NULL,
	"email" varchar(64) NOT NULL,
	"username" varchar(64) NOT NULL,
	"pwd" varchar(255) NOT NULL,
	"credits" int4 NOT NULL DEFAULT 0,
	"weekly_income" int4 NOT NULL DEFAULT 0,
	PRIMARY KEY("person_id"),
	CONSTRAINT "person_unique_username" UNIQUE("username"),
	CONSTRAINT "person_unique_email" UNIQUE("email"),
	CONSTRAINT "person_positive_credits" CHECK(credits >= 0),
	CONSTRAINT "person_positive_income" CHECK(weekly_income >= 0)
);

CREATE TABLE "role" (
	"role_id" SERIAL NOT NULL,
	"name" varchar(64) NOT NULL,
	"description" varchar(255),
	PRIMARY KEY("role_id"),
	CONSTRAINT "role_unique_name" UNIQUE("name")
);

CREATE TABLE "room" (
	"room_id" SERIAL NOT NULL,
	"room_name" varchar(64) NOT NULL,
	"room_capacity" int4 NOT NULL,
	"room_available" bool NOT NULL DEFAULT True,
	"price_base" int4,
	PRIMARY KEY("room_id"),
	CONSTRAINT "room_capacity_range" CHECK(room_capacity >= 0),
	CONSTRAINT "room_positive_price_base" CHECK(price_base >= 0),
	CONSTRAINT "room_unique_name" UNIQUE("room_name")
);

CREATE TABLE "reservation" (
	"reservation_id" SERIAL NOT NULL,
	"date" date NOT NULL,
	"note" varchar(128),
	"status" varchar(32) NOT NULL DEFAULT 'UNKNOWN',
	"person_id" int4 NOT NULL,
	"room_id" int4 NOT NULL,
	"cost" int4 NOT NULL,
	PRIMARY KEY("reservation_id"),
  CONSTRAINT "cost" CHECK(cost >= 0)
);

CREATE TABLE "review" (
	"review_id" SERIAL NOT NULL,
	"rating" int4 NOT NULL,
	"review_text" varchar(64),
	"review_date" timestamp with time zone,
	"person_id" int4 NOT NULL,
	"room_id" int4 NOT NULL,
	PRIMARY KEY("review_id"),
	CONSTRAINT "review_rating_range" CHECK(rating >= 1 and rating <= 5)
);

CREATE TABLE "equipment" (
	"equipment_id" SERIAL NOT NULL,
	"description" varchar(255) NOT NULL,
	"equipment_available" bool NOT NULL DEFAULT True,
	"status" varchar(255),
	"room_id" int4 NOT NULL,
	"equipment_type_id" int4 NOT NULL,
	PRIMARY KEY("equipment_id")
);

CREATE TABLE "news" (
	"news_id" SERIAL NOT NULL,
	"news_datetime" timestamp with time zone NOT NULL,
	"title" varchar(255) NOT NULL,
	"news_text" text NOT NULL,
	"person_id" int4 NOT NULL,
	PRIMARY KEY("news_id")
);

CREATE TABLE "equipmenttype" (
	"equipment_type_id" SERIAL NOT NULL,
	"name" varchar(64) NOT NULL,
	"added_credit_value" int4 NOT NULL DEFAULT 0,
	PRIMARY KEY("equipment_type_id"),
	CONSTRAINT "equipment_type_unique_name" UNIQUE("name"),
	CONSTRAINT "equipment_type_positive_added_value" CHECK(added_credit_value >= 0)
);

CREATE TABLE "post" (
	"post_id" SERIAL NOT NULL,
	"person_id" int4 NOT NULL,
	"room_id" int4,
	"title" varchar(255) NOT NULL,
	"text" text NOT NULL,
	PRIMARY KEY("post_id")
);

CREATE TABLE "pricemodifier" (
	"price_modifier_id" SERIAL NOT NULL,
	"day_of_week" int4 NOT NULL,
	"price_modifier" float4 NOT NULL DEFAULT 1.0,
	"timewindow_id" int4 NOT NULL,
	"room_id" int4 NOT NULL,
	PRIMARY KEY("price_modifier_id"),
	CONSTRAINT "price_modifier_unique_day_time_room" UNIQUE("day_of_week","timewindow_id","room_id"),
	CONSTRAINT "day_of_week_1_to_7" CHECK(day_of_week >= 1 AND day_of_week <=7)
);

CREATE TABLE "timewindow" (
	"timewindow_id" SERIAL NOT NULL,
	"from" time NOT NULL,
	"to" time NOT NULL,
	PRIMARY KEY("timewindow_id")
);

CREATE TABLE "permission" (
	"permission_id" SERIAL NOT NULL,
	"name" varchar(32) NOT NULL,
	PRIMARY KEY("permission_id"),
	CONSTRAINT "permission_unique_name" UNIQUE("name")
);

CREATE TABLE "role_permission" (
	"role_id" int4 NOT NULL,
	"permission_id" int4 NOT NULL,
	PRIMARY KEY("role_id", "permission_id")
);

CREATE TABLE "reservation_timewindow" (
	"reservation_id" int4 NOT NULL,
	"timewindow_id" int4 NOT NULL,
	PRIMARY KEY("reservation_id","timewindow_id")
);

CREATE TABLE "person_role" (
	"person_id" int4 NOT NULL,
	"role_id" int4 NOT NULL,
	PRIMARY KEY("person_id","role_id")
);

ALTER TABLE "reservation_timewindow" ADD CONSTRAINT "ref_reservation_timewindow_to_reservation" FOREIGN KEY ("reservation_id")
	REFERENCES "reservation"("reservation_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "reservation_timewindow" ADD CONSTRAINT "ref_reservation_timewindow_to_timewindow" FOREIGN KEY ("timewindow_id")
	REFERENCES "timewindow"("timewindow_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "role_permission" ADD CONSTRAINT "ref_role_permission_to_role" FOREIGN KEY ("role_id")
	REFERENCES "role"("role_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "role_permission" ADD CONSTRAINT "ref_role_permission_to_permission" FOREIGN KEY ("permission_id")
	REFERENCES "permission"("permission_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "reservation" ADD CONSTRAINT "reservation_person" FOREIGN KEY ("person_id")
	REFERENCES "person"("person_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "reservation" ADD CONSTRAINT "reservation_room" FOREIGN KEY ("room_id")
	REFERENCES "room"("room_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "review" ADD CONSTRAINT "review_person" FOREIGN KEY ("person_id")
	REFERENCES "person"("person_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "review" ADD CONSTRAINT "review_room" FOREIGN KEY ("room_id")
	REFERENCES "room"("room_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "equipment" ADD CONSTRAINT "equipment_room" FOREIGN KEY ("room_id")
	REFERENCES "room"("room_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "equipment" ADD CONSTRAINT "equipment_type" FOREIGN KEY ("equipment_type_id")
	REFERENCES "equipmenttype"("equipment_type_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "news" ADD CONSTRAINT "news_person" FOREIGN KEY ("person_id")
	REFERENCES "person"("person_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "post" ADD CONSTRAINT "post_room" FOREIGN KEY ("room_id")
	REFERENCES "room"("room_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "post" ADD CONSTRAINT "post_person" FOREIGN KEY ("person_id")
	REFERENCES "person"("person_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "pricemodifier" ADD CONSTRAINT "pricemodifier_room" FOREIGN KEY ("room_id")
	REFERENCES "room"("room_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "pricemodifier" ADD CONSTRAINT "pricemodifier_timewindow" FOREIGN KEY ("timewindow_id")
	REFERENCES "timewindow"("timewindow_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "person_role" ADD CONSTRAINT "ref_person_role_to_person" FOREIGN KEY ("person_id")
	REFERENCES "person"("person_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;

ALTER TABLE "person_role" ADD CONSTRAINT "ref_person_role_to_role" FOREIGN KEY ("role_id")
	REFERENCES "role"("role_id")
	MATCH SIMPLE
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	NOT DEFERRABLE;


